---
layout: article
titles:
  en: About
author: Philo
key: page-about
---

This is a blog without any theme or agenda.  I'm writing it for myself just to
make sure I'm still literate, so don't take anything here seriously.  Whatever
it is, it represents minimal effort.  I'm trying to turn my half-baked thoughts
into something I can read, but that doesn't mean I'll write anything actually
*worth* reading.
